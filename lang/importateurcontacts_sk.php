<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/importateurcontacts?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_fournisseur_active_avec_moteur' => 'Aktivovať cez @moteur@',
	'configurer_fournisseur_desactive' => 'Deaktivovať',
	'configurer_titre' => 'Nastaviť služby na nahrávanie kontaktov',

	// E
	'email_liste' => 'Zoznam e-mailov',
	'email_simple' => 'Jednoduchý e-mail',
	'erreur_aucun_fournisseur' => 'Žiadna služba vám teraz nemôže zabezpečiť, aby ste našli svoje kontakty. Nainštalujte si, prosím, aspoň jednu knižnicu na nahrávanie kontaktov.',
	'erreur_aucun_fournisseur_configure' => 'Neaktivovali ste žiadnu službu na nahrávanie kontaktov.',
	'explications_liste_email' => 'Zoznam adries zadajte oddelením každej adresy na prázdny riadok.

Akceptovaných je viac formátov: "Meno človeka &lt;email@domaine.tld&gt;", "couriel@domaine.tld Meno človeka", "couriel@domaine.tld".
',

	// I
	'importer_bouton_recuperer_contacts' => 'Získať kontakty',
	'importer_fournisseur_label' => 'Vyberte si službu, z ktorej chcete získať svoje kontakty',
	'info_aucun_contact' => 'Pre tento účet sa nenašiel žiaden kontakt.',
	'info_nb_contacts' => 'Získali ste @nb@ kontaktov.',
	'info_nb_contacts_erreur' => 'Získali ste @nb@ kontaktov, ale nedali sa použiť',
	'info_titre' => 'Nahrávač kontaktov'
);
