<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-importateur_contacts?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'importateur_contacts_description' => 'Lets you import contacts from one or more web services.',
	'importateur_contacts_nom' => 'Contacts importer',
	'importateur_contacts_slogan' => 'Contacts import form.'
);
