<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/importateur_contacts/lang
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_fournisseur_active_avec_moteur' => 'Activé avec @moteur@',
	'configurer_fournisseur_desactive' => 'Désactivé',
	'configurer_titre' => 'Configurer les services d’importation de contacts',

	// E
	'email_liste' => 'Une liste d’email',
	'email_simple' => 'Un simple email',
	'erreur_aucun_fournisseur' => 'Aucun service pouvant fournir des contacts n’a été trouvé. Veuillez installer au moins une librairie d’importation de contacts.',
	'erreur_aucun_fournisseur_configure' => 'Aucun service d’import de contacts n’est activé.',
	'explications_liste_email' => 'Saisissez une liste d’adresses en en indiquant une seule par ligne.

Plusieurs formats sont acceptés : "Nom de la personne &lt;email@domaine.tld&gt;", "couriel@domaine.tld Nom de la personne", "couriel@domaine.tld".
',

	// I
	'importer_bouton_recuperer_contacts' => 'Récupérer mes contacts',
	'importer_fournisseur_label' => 'Choisissez le service avec lequel récupérer vos contacts',
	'info_aucun_contact' => 'Aucun contact n’a été trouvé pour ce compte.',
	'info_nb_contacts' => '@nb@ contacts ont été récupérés.',
	'info_nb_contacts_erreur' => '@nb@ contacts ont été récupérés mais n’ont pu être utilisés.',
	'info_titre' => 'Importateur de contacts'
);
