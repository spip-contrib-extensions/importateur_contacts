<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-importateur_contacts?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'importateur_contacts_description' => 'Umožní vám nahrať kontakty z viacerých internetových služieb.',
	'importateur_contacts_nom' => 'Nahrávač kontaktov',
	'importateur_contacts_slogan' => 'Formulár na nahrávanie kontaktov'
);
