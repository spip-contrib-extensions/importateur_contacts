<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/importateur_contacts/lang
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'importateur_contacts_description' => 'Permet d’importer des contacts à l’aide d’un ou plusieurs services web.',
	'importateur_contacts_nom' => 'Importateur de contacts',
	'importateur_contacts_slogan' => 'Formulaire d’importation de contacts.'
);
