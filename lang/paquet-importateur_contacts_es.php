<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-importateur_contacts?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'importateur_contacts_description' => 'Permite importar contactos para ayudar a uno o a varios servicios web.',
	'importateur_contacts_nom' => 'Importador de contactos',
	'importateur_contacts_slogan' => 'Formulario de importación de contactos.'
);
